@extends('layouts.app')

@section('content')
    @php
        /** @import \App\Models\User $users - Объект класса User */ @endphp
    <form method="POST" action="{{ route('users.update', $users->id) }}">
        @method('PATCH')
        @csrf
        <div class="container">
            @php
                /** @import \Illuminate\Support\ViewErrorBag $errors */
            @endphp
            {{-- Если в переменной $errors что то есть --}}
            @if ($errors->any())
                {{-- Выполняем этот блок кода --}}
                <div class="row justify-content-center">
                    <div class="col-md-11">
                        <div class="alert alert-danger" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">x</span>
                            </button>
                            {{-- С помощью метода first() получаем первую ошибку из списка --}}
                            {{$errors->first()}}
                        </div>
                    </div>
                </div>
            @endif

            @if (session('success'))
                <div class="row justify-content-center">
                    <div class="col-md-11">
                        <div class="alert alert-success" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">x</span>
                            </button>
                            {!! session()->get('success') !!}
                        </div>
                    </div>
                </div>
            @endif

            <div class="row justify-content-center">
                <div class="col-md-8">
                    @include('admin.users.includes.user_edit_main_col')
                </div>
                <div class="col-md-3">
                    @include('admin.users.includes.user_edit_add_col')
                </div>
            </div>
        </div>
    </form>
@endsection
