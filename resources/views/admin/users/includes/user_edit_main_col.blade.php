@php
    /** @var \App\Models\User $users - Объект класса User */
    /** @var \Illuminate\Support\Collection $usersList */
@endphp
<div class="row justify-content-center">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="card-title"></div>
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#maindata" role="tab">Данные пользователя</a>
                    </li>
                </ul>
                <br>
                <div class="tab-content">
                    <div class="tab-pane active" id="maindata" role="tabpanel">
                        <div class="form-group">
                            <label for="name">Имя</label>
                            <input name="name" value="{{ $users->name }}"
                                   id="name"
                                   type="text"
                                   class="form-control"
                                   minlength="3"
                                   required>
                        </div>

                        <div class="form-group">
                            <label for="email">EMAIL</label>
                            <input name="email" value="{{ $users->email }}"
                                   id="email"
                                   type="text"
                                   class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="password">Пароль</label>
                            <input name="password" value="{{ $users->password }}"
                                   id="password"
                                   type="text"
                                   class="form-control">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
