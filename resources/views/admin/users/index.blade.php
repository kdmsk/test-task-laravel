@extends('layouts.app')

@section('content')
    <div class="container">
        @php
            /** @import \Illuminate\Support\ViewErrorBag $errors */
        @endphp
        {{-- Если в переменной $errors что то есть --}}
        @if ($errors->any())
            {{-- Выполняем этот блок кода --}}
            <div class="row justify-content-center">
                <div class="col-md-11">
                    <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">x</span>
                        </button>
                        {{-- С помощью метода first() получаем первую ошибку из списка --}}
                        {{$errors->first()}}
                    </div>
                </div>
            </div>
        @endif

        @if (session('success'))
            <div class="row justify-content-center">
                <div class="col-md-11">
                    <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">x</span>
                        </button>
                        {!! session()->get('success') !!}
                    </div>
                </div>
            </div>
        @endif
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card stripped-tabled-with-hover">
                <div class="card-header ">
                    <h4 class="card-title">
                        <!-- Кнопка для добавления пользователей -->
                        <a href="{{ route('users.create') }}" class="btn btn-primary">Добавить</a>
                    </h4>
                </div>
                <div class="card-body table-full-width table-responsive">
                    <table class="table table-hover table-striped">
                        <thead>
                        <th>#</th>
                        <th>Имя</th>
                        <th>Почта</th>
                        <th>Пароль</th>
                        <th></th>
                        </thead>
                        <tbody>
                        @foreach ($users as $user)

                            <tr>
                                <td>{{ $user->id }}</td>
                                <td>
                                    <a href="{{ route('users.edit', $user->id) }}">
                                        {{ $user->name }}
                                    </a>
                                </td>
                                <td>
                                    {{ $user->email }}
                                </td>
                                <td>
                                    {{ $user->password }}
                                </td>
                                <td class="table-buttons">
                                    <a href="{{ route('users.edit', $user->id) }}" class="btn btn-primary">
                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                    </a>
                                    <form action="{{ route('users.destroy', $user->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">
                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
