@extends('layouts.app')

@section('content')
    @php
        /** @import \App\Models\User $users - Объект класса User */ @endphp
    <form method="POST" action="{{ route('users.store')}}">
        @csrf
        <div class="container">
            @php
                /** @import \Illuminate\Support\ViewErrorBag $errors */
            @endphp
            {{-- Если в переменной $errors что то есть --}}
            @if ($errors->any())
                {{-- Выполняем этот блок кода --}}
                <div class="row justify-content-center">
                    <div class="col-md-11">
                        <div class="alert alert-danger" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">x</span>
                            </button>
                            {{-- С помощью метода first() получаем первую ошибку из списка --}}
                            {{$errors->first()}}
                        </div>
                    </div>
                </div>
            @endif

            @if (session('success'))
                <div class="row justify-content-center">
                    <div class="col-md-11">
                        <div class="alert alert-success" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">x</span>
                            </button>
                            {!! session()->get('success') !!}
                        </div>
                    </div>
                </div>
            @endif

            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-title"></div>
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#maindata" role="tab">Добавление пользователя</a>
                                </li>
                            </ul>
                            <br>
                            <div class="tab-content">
                                <button type="submit" class="btn btn-primary">Добавить</button>
                                <div class="tab-pane active" id="maindata" role="tabpanel">
                                    <div class="form-group">
                                        <label for="name">Имя</label>
                                        <input name="name" value="{{ old('name') }}"
                                               id="name"
                                               type="text"
                                               class="form-control"
                                               minlength="3"
                                               required>
                                    </div>

                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input name="email" value="{{ old('email')  }}"
                                               id="email"
                                               type="text"
                                               class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label for="password">Пароль</label>
                                        <input name="password" value="{{ old('password')  }}"
                                               id="password"
                                               type="text"
                                               class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
