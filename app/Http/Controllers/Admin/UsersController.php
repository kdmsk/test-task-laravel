<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use SebastianBergmann\CodeCoverage\Report\Xml\Method;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View|Response
     */
    public function index()
    {
        $users = User::all();

        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|void
     */
    public function create()
    {
        return view('admin.users.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'password' => 'required'
        ]);
        // Записываем в БАЗУ
        User::create($request->all());

        return redirect()->route('users.index')
            ->with('success', 'Пользователь добавлен.');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return void
     */
    public function show(int $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Application|Factory|View|Response
     */
    public function edit(int $id)
    {
        $users = User::query()->findOrFail($id);
        $usersList = User::all();

        return view('admin.users.edit',
            compact('users', 'usersList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Request $request, int $id)
    {

        // Делаем запрос в БД через класс модели User
        $users = User::query()->find($id);
        // Если данные не найдены
        if (empty($users)) {
            // Возвращаемся назад
            return back()
                // Сообщение об ошибке
                ->withErrors(['msg' => "Пользователь с id=[{$id}] не найден"])
                // Возвращаем введенные данные (текс в инпуте)
                ->withInput();
        }
        // В переменную $data получаем массив всех данных пришедших с $request
        $data = $request->all();

        $result = $users
            // Обновляем свойства объекта с помощью метода fill
            ->fill($data)
            // Записываем в БАЗУ с помощью метода save()
            ->save();
        // Если успешный результат ($result = true)
        if ($result) {
            return redirect()
                // Возвращаем представление по заданному маршруту
                ->route('users.edit', $users->id)
                // С помощью метода with() отправляем сообщение
                ->with(['success' => 'Успешно сохранено']);
        } else {
            // Иначе получаем сообщение с ошибкой
            return back()
                ->withErrors(['msg' => 'Ошибка сохранения'])
                ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse
     * @throws Exception
     */
    public function destroy($id)
    {
        // Делаем запрос в БД через класс модели User
        $user = User::query()->find($id);
        // Удаляем пользователя из БД
        $user->delete();

        // Возвращаем страницу со списком пользователей
        return redirect()->route('users.index')
            // И сообщение об успешном добавлении
            ->with('success', 'Пользователь удален');
    }
}
